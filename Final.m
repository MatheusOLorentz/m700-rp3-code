% Clear memory
clear all
close all
clc

%% Original image import

Img_original = imread('smile_original.bmp');

imshow(Img_original)
%title('Original Image')
saveas(gcf,'matlab_Original_img.png')

%% Processing image

% Recize the image to 384 x 384, to have 3 layers
Img_resized = double(imresize(Img_original,[384,384],'nearest'));

% Open a new figure and show the image after resize
figure
imshow(Img_resized)
%title('Resized image')
saveas(gcf,'matlab_Resized_img.png')

%% Converting to print head pattern, will result in a "negative" image
%changes data 0->1 and 1->0 and create the images divided in 3 layers

Img_Xaar = zeros(384);
Img_layer1 = zeros(384,128);
Img_layer2 = zeros(384,128);
Img_layer3 = zeros(384,128);

for i = 1:384
    for j = 1:384
        if Img_resized(i,j) == 1
            Img_Xaar(i,j) = 0;
        else
            if Img_resized(i,j) == 0
                Img_Xaar(i,j) = 1;
            end
        end
        if j <= 128
            Img_layer1(i,j) = Img_Xaar(i,j);
        end
        
        if j > 128 && j <= 256
            Img_layer2(i,j-128) = Img_Xaar(i,j);
        end
        
        if j > 256 && j <= 384
            Img_layer3(i,j-256) = Img_Xaar(i,j);
        end
    end
end
clear i;
clear j;

% Print and save the created images
figure
imshow(Img_Xaar)
%title('Image in the  Xaar patter')
saveas(gcf,'matlab_to_Xaar.png')

figure
imshow(Img_layer1)
%title('First layer')
saveas(gcf,'matlab_layer1.png')

figure
imshow(Img_layer2)
%title('Second layer')
saveas(gcf,'matlab_layer2.png')

figure
imshow(Img_layer3)
%title('Third layer')
saveas(gcf,'matlab_layer3.png')

% Save the Img_Xaar in a Excel file
xlswrite('smile_binary.xlsx',Img_Xaar)

%% Operations with the layers to create the G-Code file

M700 = zeros(1,128);

a = -tan(deg2rad(31));

dy_px = tan(deg2rad(31));
dx_px = 1;

px = 9/128;
dX = px;
dY = 9;

X_inic = 177.96;
Y_inic = 233.7;

X = X_inic - (size(Img_Xaar,1) * dX);
Y = Y_inic - (size(Img_Xaar,1) * dX);


%% Machine Setup

fileID = fopen('G-code_exemple.txt','w');
fprintf(fileID, '\n; Descri��o do fatiador');
fprintf(fileID, '\n; Altura de camada do material 1: 0.5mm');
fprintf(fileID, '\n; Altura de camada do material 2: 0.6mm');

fprintf(fileID, '\n\n; Home de todos os eixos');

fprintf(fileID, '\nG28');
fprintf(fileID, '\nG28 U V W0 W1 W2');
fprintf(fileID, '\n\n; Set de velocidades');
fprintf(fileID, '\nM20 S 100 F 200 S 200 O 20');

fprintf(fileID, '\n\n; Constru��o da base para impress�o');
fprintf(fileID, '\nG1 W 0.5');
fprintf(fileID, '\nM10 1 1');
fprintf(fileID, '\nG1 W 1.0');
fprintf(fileID, '\nM10 1 1');
fprintf(fileID, '\nG1 W 1.5');
fprintf(fileID, '\nM10 1 1');
fprintf(fileID, '\nG1 W 2.0');
fprintf(fileID, '\nM10 1 2');

fprintf(fileID, '\n\n; Desloca o cabe�ote para o in�cio da imagem');
fprintf(fileID, '\nG0 X %.3f Y %.3f',X,Y);
fprintf(fileID, '\n\n; Come�a a impress�o da primeira faixa');

%% First slice

for j = 1:dy_px:500
    n = 0;
    M700 = zeros(1,128);
    for i = 1:128
        y = a * i + j;
        if (int16(y) >= 1 && int16(y) <= 384)
            M700(1,i) = Img_layer1(int16(y),i);
            if (M700(1,i) == 1)
                n = n + 1;
            end
        end
    end
    %display(M700);
    X = X + dX;
    if (n > 0)
        fprintf(fileID,'\nG1 X %.3f ',X); 
        fprintf(fileID,'\nM700 '); fprintf(fileID, '%d',M700);
    end
end
Y = Y + dY;
X = X_inic - (size(Img_Xaar,1) * dX)/3;
fprintf(fileID, '\n; Conclu�da a impress�o da primeira faixa\n\n Deslocamento para a posi��o da segunda faixa');
fprintf(fileID,'\nG1 X %.3f Y %.3f',X,Y);
for j = 1:dy_px:500
    n = 0;
    M700 = zeros(1,128);
    for i = 1:128
        y = a * i + j;
        if y >= 1 && y <= 384
            M700(1,i) = Img_layer2(int16(y),i);
        end
        if M700(1,i) == 1
            n = n + 1;
        end
        
    end
    X = X + dX;
    if n>0
        fprintf(fileID,'\nG1 X %.3f ',X);  
        fprintf(fileID,'\nM700 '); fprintf(fileID, '%d',M700); 
    end
end
Y = Y + dY;
X = X_inic + (size(Img_Xaar,1) * dX)/3;
fprintf(fileID, '\n; Conclu�da a impress�o da segunda faixa\n\n Deslocamento para a posi��o da terceira faixa');
fprintf(fileID,'\nG1 X %.3f Y %.3f',X,Y);
for j = 1:dy_px:500
    n = 0;
    M700 = zeros(1,128);
    for i = 1:128
        y = a * i + j;
        if y >= 1 && y <= 384
            M700(1,i) = Img_layer3(int16(y),i);
        end
        if M700(1,i) == 1
            n = n + 1;
        end
        
    end
    X = X + dX;
    if n>0
        fprintf(fileID,'\nG1 X %.3f ',X); 
        fprintf(fileID,'\nM700 '); fprintf(fileID, '%d',M700); 
    end
end
%% Second slice
X = X_inic - (size(Img_Xaar,1) * dX);
Y = Y_inic - (size(Img_Xaar,1) * dX);

fprintf(fileID,'\n; Conclu�da a primeira camada da impress�o\n\n; Desloca os eixos X e Y para uma posi��o segura para o espalhamento da segunda camada de material');
fprintf(fileID,'\nG0 X 0.5 Y 497');
fprintf(fileID,'\n\n; Defini��o de qual o material a ser usado na pr�xima camada e espalhamento do material');
fprintf(fileID,'\nM10 2 1');
fprintf(fileID,'\nG1 W 0.6');
fprintf(fileID,'\n\n; Desloca para o inicio da imagem');
fprintf(fileID, '\nG0 X %.3f Y %.3f',X,Y);
fprintf(fileID,'\n\n; Come�a a impress�o da primeira feixa');

for j = 1:dy_px:500
    n = 0;
    M700 = zeros(1,128);
    for i = 1:128
        y = a * i + j;
        if (int16(y) >= 1 && int16(y) <= 384)
            M700(1,i) = Img_layer1(int16(y),i);
            if (M700(1,i) == 1)
                n = n + 1;
            end
        end
    end
    %display(M700);
    X = X + dX;
    if (n > 0)
        fprintf(fileID,'\nG1 X %.3f ',X); 
        fprintf(fileID,'\nM700 '); fprintf(fileID, '%d',M700);
    end
end
Y = Y + dY;
X = X_inic - (size(Img_Xaar,1) * dX)/3;
fprintf(fileID, '\n; Conclu�da a impress�o da primeira faixa\n\n Deslocamento para a posi��o da segunda faixa');
fprintf(fileID,'\nG1 X %.3f Y %.3f',X,Y);
for j = 1:dy_px:500
    n = 0;
    M700 = zeros(1,128);
    for i = 1:128
        y = a * i + j;
        if y >= 1 && y <= 384
            M700(1,i) = Img_layer2(int16(y),i);
        end
        if M700(1,i) == 1
            n = n + 1;
        end
        
    end
    X = X + dX;
    if n>0
        fprintf(fileID,'\nG1 X %.3f ',X); 
        fprintf(fileID,'\nM700 '); fprintf(fileID, '%d',M700); 
    end
end
Y = Y + dY;
X = X_inic + (size(Img_Xaar,1) * dX)/3;
fprintf(fileID, '\n; Conclu�da a impress�o da segunda faixa\n\n Deslocamento para a posi��o da terceira faixa');
fprintf(fileID,'\nG1 X %.3f Y %.3f',X,Y);
for j = 1:dy_px:500
    n = 0;
    M700 = zeros(1,128);
    for i = 1:128
        y = a * i + j;
        if y >= 1 && y <= 384
            M700(1,i) = Img_layer3(int16(y),i);
        end
        if M700(1,i) == 1
            n = n + 1;
        end
        
    end
    X = X + dX;
    if n>0
        fprintf(fileID,'\nG1 X %.3f ',X); 
        fprintf(fileID,'\nM700 '); fprintf(fileID, '%d',M700); 
    end
end
fprintf(fileID,'\nG28');
fprintf(fileID,'\n\n; Iniciando a rotina de prote��o da pe�a');
fprintf(fileID,'\nG1 W 3.1');
fprintf(fileID,'\nM10 1 1');
fprintf(fileID,'\nG1 W 3.6');
fprintf(fileID,'\nM10 1 1');
fprintf(fileID,'\nG1 W 4.1');
fprintf(fileID,'\nM10 1 1');
fprintf(fileID,'\nG1 W 4.6');
fprintf(fileID,'\nM10 1 1');
fprintf(fileID,'\nG28 U V');
fclose(fileID);

clear ans
clear fileID

close all
display('Done')